import { NextResponse } from 'next/server'
import { google } from "googleapis";

interface SheetData {
    email: string;
    date: string;
}

export async function POST(request: Request) {

    const body: SheetData = await request.json()
    
    try {
        const auth = new google.auth.GoogleAuth({
            credentials: {
                client_email: process.env.GOOGLE_CLIENT_EMAIL,
                private_key: process.env.GOOGLE_PRIVATE_KEY?.replace(/\\n/g, '\n')
            },
            scopes: [
                'https://www.googleapis.com/auth/drive',
                'https://www.googleapis.com/auth/drive.file',
                'https://www.googleapis.com/auth/spreadsheets'
            ]
        })

        const sheets = google.sheets({
            auth,
            version: 'v4'
        });

        const res = await sheets.spreadsheets.values.append({
            spreadsheetId: process.env.GOOGLE_SHEET_ID,
            range: process.env.GOOGLE_SHEET_RANGE,
            valueInputOption: 'USER_ENTERED',
            requestBody: {
                values: [
                    [body?.email, body?.date]
                ]
            }
        });

        if (res.status !== 200) {
            return NextResponse.json({"message": "error creating entry"}, {status: res.status})
        }

        return NextResponse.json({"message": "success"})

    } catch (e: any) {

        return NextResponse.json({"message": e.message}, {status: 500})

    }

}