'use client'
import { useEffect, useState } from 'react'
import { ActionIcon, Flex, Loader, Button, Card, Grid, Image, Stack, Text, TextInput } from '@mantine/core'
import { notifications } from '@mantine/notifications'
import { IconAlertOctagon, IconMail, IconX } from '@tabler/icons-react'
import { GoogleAnalytics } from 'nextjs-google-analytics'
import Link from 'next/link'

import "./page.css"

export interface Submission {
  email: string;
  date: string;
}

export default function Home() {

  const [submission, setSubmission] = useState<Submission>({email: '', date: ''})
  const [submitting, setSubmitting] = useState<boolean>(false)
  const [success, setSuccess] = useState<boolean>(false)
  const [automaticDateIncrement, setAutomaticDateIncrement] = useState<string | null>(null)

  const generateDate = () => {

    const month = new Date().getMonth() + 1
    const year = new Date().getFullYear().toString().substring(2,4)

    return `${month}.${year}`
  }

  const handleSubmit = async (submission: Submission) => {

    console.log("handling submit")

    let emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    
    if (!emailRegex.test(submission.email) || submission.email ===  '') {

      console.log("invalid email address")

      notifications.show({
        color: 'red',
        title: 'Invalid email address',
        message: 'Please enter a valid email address.',
        icon: <IconAlertOctagon />
      })
      return
    }

    setSubmitting(true)

    submission.date = new Date().toISOString()

    const rawResponse = await fetch(process.env.NEXT_PUBLIC_API_PATH ? process.env.NEXT_PUBLIC_API_PATH : "/api/save", {
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(submission)
    });
    const content = await rawResponse.json();
    
    setSubmitting(false)

    if (content.message === "success") {
      setSuccess(true)
    } else {
      notifications.show({
        color: 'red',
        title: 'Error',
        message: 'An error occurred. Please try again later.',
        icon: <IconAlertOctagon />
      })
    }

  }

  const handleClear = () => {

    setSubmission({email: '', date: ''})

  }

  useEffect (() => {

    if (process.env.NEXT_PUBLIC_USE_AUTOMATIC_DATE_INCREMENT) {
      setAutomaticDateIncrement("(" + generateDate() + ")")
    }

  }, [])

  return (
      <>
      <GoogleAnalytics trackPageViews />

      <Grid justify="center">

          <Grid.Col span={12} style={{textAlign: "center"}}>
            <Card radius={20} withBorder shadow='sm' style={{display:"inline-block", marginTop: 75, minWidth:300}} maw={500}>
              <Flex
                  direction="column"
                  justify="center"
                  align="center"
              >                  
                <Image style={{display:"inline-flex"}} src="/logo.png" alt="logo" w={150} h={150}/>
              </Flex>
              <Text fz={35} fw={800}>
                {process.env.NEXT_PUBLIC_PAGE_TITLE ? process.env.NEXT_PUBLIC_PAGE_TITLE : "Not Set"}
              </Text>
              <Link className="pageLink" href={"mailto:" + (process.env.NEXT_PUBLIC_CONTACT_EMAIL ? process.env.NEXT_PUBLIC_CONTACT_EMAIL : "not-set@example.com")}>
                <Text fw={200}>
                  {process.env.NEXT_PUBLIC_CONTACT_EMAIL ? process.env.NEXT_PUBLIC_CONTACT_EMAIL : "not-set@example.com"}
                </Text>
              </Link>
              <Text fz={12} fw={100} style={{marginTop: 5}}>
                {process.env.NEXT_PUBLIC_APP_VERSION ? process.env.NEXT_PUBLIC_APP_VERSION : "Alpha"} {automaticDateIncrement}
              </Text>
            </Card>
          </Grid.Col>

          <Grid.Col span={success ? 12 : 12} style={{textAlign: "center", paddingTop: 50}}>
            { 
              submitting ?
                <Loader color="white"/>
              :
                success ?
                  <Card radius={20} style={{display:"inline-block", minWidth:300}} maw={300} withBorder shadow="sm">
                    <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                      <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                      <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                    </svg>
                    <Card.Section style={{paddingBottom: 10}}>
                      <Text>
                        Thank you for joining the waitlist. Your interest is greatly appreciated.
                      </Text>
                      <Text>
                        We will be in touch very soon.
                      </Text>
                    </Card.Section>
                  </Card>
                :  
                  <Stack maw={300} style={{margin:"auto"}}>
                    <TextInput
                      leftSection={<IconMail />}
                      rightSection={
                        submission?.email.length > 0 && 
                          <ActionIcon
                            variant='transparent'
                            onClick={handleClear}
                          >
                            <IconX />
                          </ActionIcon>
                      }
                      value={submission?.email}
                      onChange={(e) => setSubmission(prevSubmission => ({ ...prevSubmission, email: e.target.value }) )}
                      type="email"
                      size='lg'
                      style={{minWidth: 300, width: "100%"}}
                      placeholder='Email address'
                    />
                  <Button
                    disabled={submission.email === ''}
                    size='lg'
                    style={{margin: 10, display:"block"}}
                    onClick={() => submission.email && handleSubmit(submission)}
                  >
                    Join the waitlist
                  </Button>
                </Stack>
            }
          </Grid.Col>
      </Grid>
      </>
  )
}
